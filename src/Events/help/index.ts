import { type ICommandObject } from '../../types';
import greeting from './greeting';

const helpCommand: ICommandObject = {
    cmd: 'help',
    label: 'Помощь',
    greeting: greeting,
};

export default helpCommand;
