import feedbackCommand from '.';
import { type ICbInfo, type TBotContext } from '../../types';
import { assessment_clear, feedback } from './assessmentKeyboard';

const assessmentCbCheck = (ctx: TBotContext, next: () => Promise<void>, cbInfo: ICbInfo) => {
    return new Promise(async resolve => {
        const { data, msgId } = cbInfo;
        if (data === ctx.session.assessment) {
            return resolve('');
        }
        const feedbackInfo = feedback.find(ass => ass.data === data);

        if (feedbackInfo) {
            if (feedbackInfo.data === assessment_clear.data) {
                ctx.session.assessment = assessment_clear.data;
            } else {
                ctx.session.assessment = feedbackInfo.data;
            }
            if (msgId) {
                try {
                    await ctx.deleteMessage(msgId);
                } catch (error) {
                    //
                }
                await feedbackCommand.greeting(ctx, next);
            }
        }
        resolve('');
    });
};

export default assessmentCbCheck;
