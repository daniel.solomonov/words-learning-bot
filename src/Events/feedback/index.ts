import { type ICommandObject } from '../../types';
import greeting from './greeting';

const feedbackCommand: ICommandObject = {
    cmd: 'feedback',
    label: 'Обратная связь',
    greeting: greeting,
};

export default feedbackCommand;
