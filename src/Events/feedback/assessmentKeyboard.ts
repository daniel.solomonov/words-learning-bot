import { Markup } from 'telegraf';
import { type InlineKeyboardMarkup } from 'telegraf/types';
import { type IButton } from '../../types';

const assessment_1: IButton = { text: '1⃣', data: 'assessment_1' };
const assessment_2: IButton = { text: '2⃣', data: 'assessment_2' };
const assessment_3: IButton = { text: '3⃣', data: 'assessment_3' };
const assessment_4: IButton = { text: '4⃣', data: 'assessment_4' };
const assessment_5: IButton = { text: '5⃣', data: 'assessment_5' };
export const assessment_clear: IButton = {
    text: 'Оцените нас 🤗',
    data: 'assessment_clear',
    alternativeText: 'Отменить оценку ❌'
};
export const feedback_msg: IButton = { text: 'Написать 📝', data: 'support_connect' };

export const assessments: IButton[] = [
    assessment_1,
    assessment_2,
    assessment_3,
    assessment_4,
    assessment_5,
];

export const feedback: IButton[] = [
    ...assessments,
    assessment_clear,
];

const assessmentKeyboard = (assessment: string): Markup.Markup<InlineKeyboardMarkup> => {
    const isAssessed = assessments.some(ass => ass.data === assessment);
    return Markup.inlineKeyboard(
        [
            ...assessments.map(ass =>
                Markup.button.callback(ass.text + (ass.data === assessment ? ' ✳' : ''), ass.data)
            ),
            Markup.button.callback(isAssessed && assessment_clear.alternativeText || assessment_clear.text, assessment_clear.data),
            Markup.button.callback(feedback_msg.text, feedback_msg.data),
        ],
        { columns: 5 },
    );
};

export default assessmentKeyboard;
