import { type TMiddleware } from '../../types';
import assessmentKeyboard, { assessments } from './assessmentKeyboard';

const greeting: TMiddleware = async (ctx) => {
    const assess = ctx.session.assessment;
    await ctx.replyWithHTML(
        `Дорогой пользователь, мы очень рады, что наш скромный телеграмм бот, `
        + `пришелся Вам ко двору, но мы не идеальны, и нам есть что исправить или добавить, `
        + `а так же нам очень важно Ваше мнение о нашей работе, мы будем рады получить `
        + `от Вас обратную связь, если Вы нашли ошибки, столкнулись с трудностями, `
        + `у Вас есть предложения или Вы просто хотите поблагодарить нас, за нашу работу, `
        + `пишите, мы обязательно прислушаемся к каждому из Вас!!!`,
        assessmentKeyboard(assess)
    );
};

export default greeting;
