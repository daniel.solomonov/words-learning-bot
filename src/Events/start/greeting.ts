import addWordScene from '../../Scenes/addWord';
import { type TMiddleware } from '../../types';
import helpCommand from '../help';

const greeting: TMiddleware = async (ctx, next) => {
    await helpCommand.greeting(ctx, next);
};

export default greeting;
