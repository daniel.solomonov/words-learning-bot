import { type ICommandObject } from '../../types';
import greeting from './greeting';

const startCommand: ICommandObject = {
    cmd: 'start',
    label: 'Старт',
    greeting: greeting,
};

export default startCommand;
