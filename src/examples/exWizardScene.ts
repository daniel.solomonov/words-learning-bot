import { Scenes } from 'telegraf';
import { type ISceneObject, type TBotContext, type TMiddleware, type TWizardScene } from '../types';

// ctx.wizard.next() — для перехода к следующему шагу;
// ctx.wizard.back() — вернуться к предыдущему шагу;
// ctx.wizard.cursor — чтобы получить индекс текущего шага;
// ctx.wizard.selectStep(index) — для перехода прямо к заданному индексу шага,
// может использоваться для реализации ветвления;

const sceneId = 'EX_WIZARD_SCENE_ID_1';

const scene: TWizardScene = new Scenes.WizardScene<TBotContext>(
  sceneId,
  async (ctx) => {
    ctx.reply('Введите ваше имя:');
    ctx.scene.session.mySceneSessionProp = '';
    // ctx.wizard.state.contactData = {};
    // или так
    // ctx.wizard.myData -> ctx.scene.myData -> ctx.session.myData
    // Если вы пойдете по этому пути, вам не придется очищать данные,
    // поскольку они очищаются автоматически, когда вы покидаете сцену или входите в новую
    // или так
    // ctx.scene.enter(sceneId, initialState)
    // и так
    // ctx.scene.enter('MY_SCENE_ID', ctx.scene.state)
    return ctx.wizard.next();
  },
  async (ctx) => {
    // validation example
    if (ctx.message.text.length < 2) {
      ctx.reply('Введите ваше настоящее имя:');
      return;
    }
    ctx.scene.session.mySceneSessionProp = ctx.message.text;
    // ctx.wizard.state.contactData.fio = ctx.message.text;
    ctx.reply('Введите ваш e-mail:');
    return ctx.wizard.next();
  },
  async (ctx) => {
    ctx.scene.session.mySceneSessionProp += ctx.message.text;
    // ctx.wizard.state.contactData.email = ctx.message.text;
    await ctx.reply('Ваша заявка принята, мы свяжемся с вами! ' + ctx.scene.session.mySceneSessionProp);
    // await mySendContactDataMomentBeforeErase(ctx.wizard.state.contactData);
    // 
    return await ctx.scene.leave();
  },
);

const greeting: TMiddleware = async (ctx) => {
  await ctx.replyWithHTML(
    'Приветствую в сценах exWizardScene greeting!!!'
  );
  await ctx.scene.enter(exWizardScene.sceneId);
};

const exWizardScene: ISceneObject = {
  cmd: 'wizard_scene',
  label: 'Показать wizard_scene',
  sceneId: sceneId,
  scene: scene,
  greeting: greeting,
};

export default exWizardScene;
