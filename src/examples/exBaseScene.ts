import { Scenes } from 'telegraf';
import Keyboard from './keyboard';
import exWizardScene from './exWizardScene';
import { type TBaseScene, type TMiddleware, type ISceneObject, type TBotContext } from '../types';

// const { enter, leave } = Scenes.Stage;
// const Base = Scenes.BaseScene<IContext<Update>>;
// const search = new Base('search');
// exampleScene.hears('Back', leave());

const sceneId = 'EX_BASE_SCENE_ID_1';

const scene: TBaseScene = new Scenes.BaseScene<TBotContext>(sceneId);

const MOVIE_ACTION = 'Movie';
const THEATER_ACTION = 'Theater';

// Вход в сценарий
scene.enter((ctx) => {
  // инициализация базы данных сценария
  ctx.scene.session.mySceneSessionProp = '';
  // или так
  // ctx.wizard.myData -> ctx.scene.myData -> ctx.session.myData
  // Если вы пойдете по этому пути, вам не придется очищать данные,
  // поскольку они очищаются автоматически, когда вы покидаете сцену или входите в новую
  // или так
  // ctx.scene.enter(sceneId, initialState)
  // и так
  // ctx.scene.enter('MY_SCENE_ID', ctx.scene.state)
  ctx.reply('Что ты выберешь?',
    Keyboard({
      btns: [
        { data: MOVIE_ACTION, text: MOVIE_ACTION },
        { data: THEATER_ACTION, text: THEATER_ACTION },
      ],
      columns: 2,
    })
  );
});

scene.action(THEATER_ACTION, (ctx) => {
  ctx.reply(`Твой выбор - ${THEATER_ACTION}`);
  // запись ответа в базу данных сценария
  ctx.scene.session.mySceneSessionProp = THEATER_ACTION;
  // переход на сценарий EXAMPLE_SCENE_ID_2
  return ctx.scene.enter(exWizardScene.sceneId);
});

scene.action(MOVIE_ACTION, async (ctx) => {
  await ctx.reply(`Твой выбор - ${MOVIE_ACTION}, ты проиграл!`);
  ctx.scene.session.mySceneSessionProp = MOVIE_ACTION;
  // выход из сценария
  return ctx.scene.leave();
});

scene.leave((ctx) => {
  ctx.reply('Попрощаеся перед выходом из сценария!');
});

scene.use((ctx) => {
  ctx.replyWithMarkdownV2('Что то пошло не по сценарию!!!');
});

const greeting: TMiddleware = async (ctx) => {
  await ctx.replyWithHTML(
    'Приветствую в сценах exBaseScene greeting!!!'
  );
  await ctx.scene.enter(exBaseScene.sceneId);
};

const exBaseScene: ISceneObject = {
  cmd: 'base_scene',
  label: 'Показать base_scene',
  sceneId: sceneId,
  scene: scene,
  greeting: greeting,
};

export default exBaseScene;
