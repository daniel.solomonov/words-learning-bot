import { Markup } from 'telegraf';
import {
    type TKeyboard, type IButton, type TResultKeyboard, type IReplyKeyboard, type IInlineKeyboard,
    type TResultReplyKeyboardMarkup, type TResultInlineKeyboardMarkup,
} from '../types';

// Markup.removeKeyboard(true)
// {reply_markup: {remove_keyboard: true}}

const keyboard = (props: TKeyboard): TResultKeyboard => {
    const { btns } = props;
    if (Array.isArray(btns[0])) {
        return getReplyKeyboard(props as IReplyKeyboard);
    }
    return getInlineKeyboard(props as IInlineKeyboard);
};

const getReplyKeyboard = ({ btns }: IReplyKeyboard): TResultReplyKeyboardMarkup => {
    return Markup.keyboard(
        btns.map(rowBtns => rowBtns.map(btn => getCallback(btn)))
    );
    // .oneTime()
    // .resize()
    // .extra()
};

const getInlineKeyboard = ({ btns, columns }: IInlineKeyboard): TResultInlineKeyboardMarkup => {
    return Markup.inlineKeyboard(
        btns.map(btn => getCallback(btn)),
        { columns }
    );
    // .oneTime()
    // .resize()
    // .extra()
};

const getCallback = (btn: IButton) => {
    const { text, data, hide } = btn;
    return Markup.button.callback(text, data, hide);
};

export default keyboard;
