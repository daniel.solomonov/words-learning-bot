import mongoose from 'mongoose';
import envParams from '../helpers/envParams';

const dbConnect = async (callback: () => Promise<void>) => {
    try {
        // mongoose.createConnection(envParams.MONGO_URI);
        mongoose.connection.on('open', () => {
            console.log('dataBase connection started!!!');
            // callback();
        });
        mongoose.connection.on('close', () => console.log('dataBase connection closed!!!'));
        mongoose.connection.on('error', async () => {
            console.log('dataBase connection error!!!');
            await mongoose.disconnect();
        });
        if (envParams.MONGO_URI) {
            await mongoose.connect(envParams.MONGO_URI, { dbName: envParams.DB_NAME });
        }
    }
    catch (err) {
        console.log('dataBase connection catch error:', err);
    }
    // finally {
    //     await mongoose.disconnect();
    // }
};

export default dbConnect;


// import mongoose from 'mongoose';
// import { config } from 'dotenv';

// config({ path: `.env.${envParams.NODE_ENV}` });

// const dbConnect = async (callback: () => Promise<void>): Promise<void> => {
//     try {
//         if (envParams.MONGO_URI) {
//             await mongoose.connect(envParams.MONGO_URI, { dbName: envParams.DB_NAME });
//             console.log('Подключаемся к базе данных');
//             await callback();
//         }
//     }
//     catch (err) {
//         console.log('dataBase connection catch error:', err);
//     }
//     finally {
//         await mongoose.disconnect();
//         console.log('Отключаемся от базы данных');
//     }
// };

// export default dbConnect;
