import { Scenes, session } from 'telegraf';
import { callbackQuery } from 'telegraf/filters';
import createBot, { initialSession } from './helpers/createBot';
import setCommands from './helpers/setCommands';
import addWordScene from './Scenes/addWord';
import { Mongo } from '@telegraf/session/mongodb';
import createStage from './helpers/createStage';
import { TBaseScene, TStage, TWizardScene, TBotContext, TSessionStore, TTelegraf, TMiddleware, ICbInfo, userDefault } from './types';
import exBaseScene from './examples/exBaseScene';
import exWizardScene from './examples/exWizardScene';
import showWordsScene from './Scenes/showWords';
import helpCommand from './Events/help';
import startCommand from './Events/start';
import feedbackCommand from './Events/feedback';
import assessmentCbCheck from './Events/feedback/assessmentCbCheck';
import supportScene from './Scenes/support';
import envParams from './helpers/envParams';
import trainingsScene from './Scenes/trainings';

const store: TSessionStore | undefined = Mongo({
    url: envParams.MONGO_URI,
    database: envParams.DB_NAME,
    collection: envParams.COLLECTION_NAME,
}) as TSessionStore | undefined;


const startBot = async () => {
    const bot: TTelegraf | undefined = createBot();
    if (bot) {
        await setCommands(bot, [
            addWordScene,
            trainingsScene,
            showWordsScene,
            helpCommand,
            feedbackCommand,
            // exBaseScene, exWizardScene
        ]);
        bot.use(session({ defaultSession: initialSession, store: store }));
        //
        const userInfo: TMiddleware = (ctx, next) => {
            ctx.session.user = ctx.message?.from || userDefault;
            next();
        };
        bot.use(userInfo);
        //
        const stage: TStage = createStage([
            addWordScene,
            trainingsScene,
            showWordsScene,
            supportScene,
            // exBaseScene, exWizardScene
        ]);
        bot.use(stage.middleware());
        // ----------------------------------------------------------------
        bot.command(startCommand.cmd, startCommand.greeting);
        bot.command(addWordScene.cmd, addWordScene.greeting);
        bot.command(trainingsScene.cmd, trainingsScene.greeting);
        bot.command(showWordsScene.cmd, showWordsScene.greeting);
        bot.command(helpCommand.cmd, helpCommand.greeting);
        bot.command(feedbackCommand.cmd, feedbackCommand.greeting);
        //
        bot.action(supportScene.cmd, supportScene.greeting);
        //
        bot.on(callbackQuery('data'), async (ctx, next) => {
            const cbInfo: ICbInfo = {
                data: ctx.callbackQuery.data,
                msgId: ctx.callbackQuery.message?.message_id,
            };
            if (cbInfo.data) {
                await assessmentCbCheck(ctx, next, cbInfo);
            }
        });
        // bot.command(exBaseScene.cmd, exBaseScene.greeting);
        // bot.command(exWizardScene.cmd, exWizardScene.greeting);
        //
        bot.use(async ctx => {
            await ctx.sendMessage('Что то пошло не по команде');
        });
        //
        bot.launch();
        console.log('bot start success');
    } else {
        console.log('bot start error');
    }
};

startBot();
