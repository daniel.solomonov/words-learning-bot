import { Context, Markup, Telegraf, Scenes, type SessionStore, type MiddlewareFn } from 'telegraf';
import { type CallbackQuery, type InlineKeyboardMarkup, type Message, type ReplyKeyboardMarkup, type Update } from 'telegraf/types';
import { type IWord, type IWordDB } from './Schemas/word';

export type TUpdate = Update.MessageUpdate<Message.TextMessage>
    | Update.CallbackQueryUpdate<CallbackQuery>;

export interface IUser {
    id: number;
    is_bot: boolean;
    first_name: string;
    username?: string;
    language_code?: string;
};

export const userDefault: IUser = {
    id: -1,
    is_bot: false,
    first_name: '',
};

export interface ISceneSession extends Scenes.WizardSessionData {
    // will be available under `ctx.scene.session.mySceneSessionProp`
    mySceneSessionProp: string;
    word: IWord;
    state: any;
};

export interface ISession extends Scenes.WizardSession<ISceneSession> {
    // will be available under `ctx.session.mySessionProp`
    mySessionProp: string;
    assessment: string;
    state: any;
    words: IWordDB[];
    user: IUser;
};

export type TScene = Scenes.SceneContextScene<TBotContext, ISceneSession>;

export type TWizard = Scenes.WizardContextWizard<TBotContext>;

export interface IContext<U extends Update = TUpdate> extends Context<U> {
    // will be available under `ctx.myContextProp`
    myContextProp: string;
    // declare session type
    session: ISession;
    // declare scene type
    scene: TScene;
    // declare wizard type
    wizard: TWizard;
};

export type TBotContext = IContext<TUpdate> & { match: RegExpExecArray };

export type TTelegraf = Telegraf<TBotContext>;

export type TSessionStore = SessionStore<ISession>;

export type TMiddleware = MiddlewareFn<TBotContext>;

export type TWizardScene = Scenes.WizardScene<TBotContext>;

export type TBaseScene = Scenes.BaseScene<TBotContext>;

export type TStage = Scenes.Stage<TBotContext, ISceneSession>;

// scene: Scenes.BaseScene<TBotContext> | Scenes.WizardScene<TBotContext>;

export interface ICommandObject {
    cmd: string;
    label: string;
    greeting: TMiddleware;
};

export interface ISceneObject extends ICommandObject {
    sceneId: string;
    scene: TWizardScene | TBaseScene;
};

export enum EMsgPos {
    prev3 = -3,
    prev2 = -2,
    prev = -1,
    current = 0,
    next = 1,
    next1 = 2,
    next2 = 3,
};

//

export interface IButton {
    text: string,
    data: string,
    alternativeText?: string,
    hide?: boolean,
};

export interface IInlineKeyboard {
    btns: IButton[],
    columns?: number,
};

export interface IReplyKeyboard {
    btns: IButton[][],
};

export type TKeyboard = IInlineKeyboard | IReplyKeyboard;

export type TResultReplyKeyboardMarkup = Markup.Markup<ReplyKeyboardMarkup>;

export type TResultInlineKeyboardMarkup = Markup.Markup<InlineKeyboardMarkup>;

export type TResultKeyboard = Markup.Markup<InlineKeyboardMarkup | ReplyKeyboardMarkup>;

export interface ICbInfo {
    data: string,
    msgId?: number,
};
