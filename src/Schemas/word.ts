import { Schema, model } from 'mongoose';

export interface IWord {
    word?: string,
    translate?: string,
    additionalTranslate?: string,
    description?: string,
    example?: string,
    progress?: number,
};

export interface IWordDB {
    id: string,
    word: string,
    translate: string,
    additionalTranslate?: string,
    description?: string,
    example?: string,
    progress?: number,
};

const wordModel = new Schema<IWordDB>({
    id: { type: String, required: true },
    word: { type: String, required: true },
    translate: { type: String, required: true },
    additionalTranslate: String,
    description: String,
    example: String,
    progress: Number,
});

const SWord = model('Word', wordModel);

export default SWord;
