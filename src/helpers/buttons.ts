import { type IButton } from '../types';

export const cancelBtn: IButton = { text: 'Отменить', data: 'cancel' };

export const backBtn: IButton = { text: 'Вернуться', data: 'back' };

export const skipBtn: IButton = { text: 'Пропустить', data: 'skip' };

export const saveBtn: IButton = { text: 'Сохранить', data: 'save' };

export const prevPageBtn: IButton = { text: 'Пред. стр.', data: 'prevPage' };

export const nextPageBtn: IButton = { text: 'След. стр.', data: 'nextPage' };

export const exitBtn: IButton = { text: 'Выйти', data: 'exit' };

export const dontKnowBtn: IButton = { text: 'Не знаю', data: 'dontKnow' };

const regEditPattern = 'editById-';

export const editRegExp = new RegExp('^' + regEditPattern + '(.*)$');

export const editBtn = (id?: string | number): IButton => ({
    text: 'Изменить',
    data: regEditPattern + id,
});

const regRemovePattern = 'removeById-';

export const removeRegExp = new RegExp('^' + regRemovePattern + '(.*)$');

export const removeBtn = (id?: string | number): IButton => ({
    text: 'Удалить',
    data: regRemovePattern + id,
});
