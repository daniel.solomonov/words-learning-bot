import { type IWordDB } from '../Schemas/word';

const displayWord = (wordObj: IWordDB): string => {
    const { word, translate, additionalTranslate, description, example } = wordObj;
    return `слово: ${word}\n`
        + `перевод: ${translate}\n`
        + (additionalTranslate ? `доп перевод: ${additionalTranslate}\n` : '')
        + (description ? `описание: ${description}\n` : '')
        + (example ? `пример: ${example}\n` : '');
};

export default displayWord;
