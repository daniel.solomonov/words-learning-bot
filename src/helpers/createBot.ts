import { Telegraf } from 'telegraf';
import { type TBotContext, type TTelegraf, type ISession, userDefault } from '../types';
import { assessment_clear } from '../Events/feedback/assessmentKeyboard';
import envParams from './envParams';

/**
 * @param {TBotContext} ctx контекст бота
 * @returns {ISession} возвращает ISession
 */
export const initialSession = (ctx: TBotContext): ISession => {
    console.log('ctx', ctx.me);
    return {
        mySessionProp: '',
        assessment: assessment_clear.data,
        state: {},
        words: [],
        user: userDefault,
    };
};

/**
 * @returns {TTelegraf} возвращает TTelegraf | undefined
 */
const createBot = (): TTelegraf | undefined => {
    if (envParams.TELEGRAM_TOKEN) {
        console.log(envParams.NODE_ENV);
        return new Telegraf<TBotContext>(envParams.TELEGRAM_TOKEN);
    }
    return undefined;
};

export default createBot;
