import { config } from 'dotenv';

config({ path: `.env.${process.env.NODE_ENV}` });

const envParams = {
    NODE_ENV: process.env.NODE_ENV ?? '',
    MY_USER_ID: process.env.MY_USER_ID ?? 1111111,
    TELEGRAM_TOKEN: process.env.TELEGRAM_TOKEN ?? '',
    PORT: process.env.PORT ?? 3000,
    MONGO_URI: process.env.MONGO_URI ?? '',
    DB_NAME: process.env.DB_NAME ?? '',
    COLLECTION_NAME: process.env.COLLECTION_NAME ?? '',
};

export default envParams;
