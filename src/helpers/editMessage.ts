import { EMsgPos, type TBotContext } from '../types';

/**
 * @param {TBotContext} ctx контекст из родительской функции
 * @param {EMsgPos} msgPos позиция сообщения
 * @param {string} message текст сообщения который заменит старый текст
 * @returns {Promise} ассинхронная функция, ничего не возвращает кроме Promise
 */
const editMessage = async (ctx: TBotContext, msgPos: EMsgPos, message: string): Promise<void> => {
    await ctx.telegram.editMessageText(
        ctx.chat?.id,
        !!ctx.message?.message_id ? ctx.message.message_id + msgPos : undefined,
        ctx.inlineMessageId,
        message,
    );
};

export default editMessage;
