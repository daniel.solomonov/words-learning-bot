import { type BotCommand } from 'telegraf/types';
import { ICommandObject, type ISceneObject, type TTelegraf } from '../types';

/**
 * @param {ISceneObject[] | ICommandObject[]} [commands] массив объектов типа ISceneObject или ICommandObject,
 * разделенных запятой
 * @returns {BotCommand} возвращает массив типа BotCommand
 */
const createCommands = (commands: ISceneObject[] | ICommandObject[]): BotCommand[] => {
    return commands.map<BotCommand>(comm => ({
        command: `/${comm.cmd}`,
        description: comm.label,
    }));
};

/**
 * @param {TTelegraf} bot созданный бот типа TTelegraf
 * @param {ISceneObject[] | ICommandObject[]} [commands] массив объектов типа ISceneObject или ICommandObject,
 * разделенных запятой
 * @returns {Promise} ассинхронная функция, ничего не возвращает кроме Promise
 */
const setCommands = async (bot: TTelegraf, commands: ISceneObject[] | ICommandObject[]): Promise<void> => {
    await bot.telegram.setMyCommands(createCommands(commands));
};

export default setCommands;
