import { type TBotContext } from '../types';

/**
 * @param {TBotContext} ctx контекст из родительской функции
 * @param {Promise} next функция next из родительской функции
 * @returns {Promise} ассинхронная функция, ничего не возвращает кроме Promise
 */
const enterToSceneStep = async (ctx: TBotContext, next: () => Promise<void>): Promise<unknown> => {
    if (typeof ctx.wizard.step === 'function') {
        return await ctx.wizard.step(ctx, next);
    }
};

/**
 * @param {TBotContext} ctx контекст из родительской функции
 * @param {Promise} next функция next из родительской функции
 * @param {Promise} callback функция Promise которая будет вызвана перед действием перехода, не обязательный параметр
 * @returns {Promise} ассинхронная функция, ничего не возвращает кроме Promise
 */
const goToStart = async (ctx: TBotContext, next: () => Promise<void>, callback?: () => Promise<void>): Promise<unknown> => {
    if (callback) {
        await callback();
    }
    ctx.wizard.selectStep(0);
    return await enterToSceneStep(ctx, next);
};

/**
 * @param {TBotContext} ctx контекст из родительской функции
 * @param {Promise} next функция next из родительской функции
 * @param {Promise} callback функция Promise которая будет вызвана перед действием перехода, не обязательный параметр
 * @returns {Promise} ассинхронная функция, ничего не возвращает кроме Promise
 */
const goToEnd = async (ctx: TBotContext, next: () => Promise<void>, callback?: () => Promise<void>): Promise<unknown> => {
    if (callback) {
        await callback();
    }
    // @ts-ignore
    const endIndex = ctx.wizard?.steps?.length - 1 ?? 0;
    ctx.wizard.selectStep(endIndex);
    return await enterToSceneStep(ctx, next);
};

/**
 * @param {TBotContext} ctx контекст из родительской функции
 * @param {Promise} next функция next из родительской функции
 * @param {Promise} callback функция Promise которая будет вызвана перед действием перехода, не обязательный параметр
 * @returns {Promise} ассинхронная функция, ничего не возвращает кроме Promise
 */
const goToPrev = async (ctx: TBotContext, next: () => Promise<void>, callback?: () => Promise<void>): Promise<unknown> => {
    if (callback) {
        await callback();
    }
    ctx.wizard.back();
    return await enterToSceneStep(ctx, next);
};

/**
 * @param {TBotContext} ctx контекст из родительской функции
 * @param {Promise} next функция next из родительской функции
 * @param {Promise} callback функция Promise которая будет вызвана перед действием перехода, не обязательный параметр
 * @returns {Promise} ассинхронная функция, ничего не возвращает кроме Promise
 */
const goToNext = async (ctx: TBotContext, next: () => Promise<void>, callback?: () => Promise<void>): Promise<unknown> => {
    if (callback) {
        await callback();
    }
    ctx.wizard.next();
    return await enterToSceneStep(ctx, next);
};

/**
 * @param {TBotContext} ctx контекст из родительской функции
 * @param {Promise} next функция next из родительской функции
 * @param {number} step индекс шага, на который нужно перейти
 * @param {Promise} callback функция Promise которая будет вызвана перед действием перехода, не обязательный параметр
 * @returns {Promise} ассинхронная функция, ничего не возвращает кроме Promise
 */
const goToStep = async (ctx: TBotContext, next: () => Promise<void>, step: number, callback?: () => Promise<void>): Promise<unknown> => {
    if (callback) {
        await callback();
    }
    ctx.wizard.selectStep(step);
    return await enterToSceneStep(ctx, next);
};

/**
 * @param {TBotContext} ctx контекст из родительской функции
 * @param {Promise} next функция next из родительской функции
 * @param {number} steps количество шагов, которые, нужно пропустить
 * @param {Promise} callback функция Promise которая будет вызвана перед действием перехода, не обязательный параметр
 * @returns {Promise} ассинхронная функция, ничего не возвращает кроме Promise
 */
const goToPlusStep = async (ctx: TBotContext, next: () => Promise<void>, steps: number, callback?: () => Promise<void>): Promise<unknown> => {
    if (callback) {
        await callback();
    }
    ctx.wizard.selectStep(ctx.wizard.cursor + steps);
    return await enterToSceneStep(ctx, next);
};

/**
 * @param {TBotContext} ctx контекст из родительской функции
 * @returns {Promise} ассинхронная функция, ничего не возвращает кроме Promise
 */
const goToExit = async (ctx: TBotContext): Promise<void> => {
    return await ctx.scene.leave();
};

export {
    goToStart,
    goToEnd,
    goToPrev,
    goToNext,
    goToStep,
    goToExit,
    goToPlusStep,
};
