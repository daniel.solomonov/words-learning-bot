import { Scenes } from 'telegraf';
import { type ISceneObject, type ISceneSession, type TStage, type TBotContext } from '../types';

/**
 * @param {ISceneObject[]} [scenes] массив объектов типа ISceneObject, разделенных запятой
 * @param {ISceneObject} defaultScene объект типа ISceneObject
 * @returns {IStage} возвращает объект типа IStage
 */
const createStage = (scenes: ISceneObject[], defaultScene?: ISceneObject): TStage => {
    return new Scenes.Stage<TBotContext, ISceneSession>(
        scenes.map(scene => scene.scene),
        { default: defaultScene?.sceneId }
    );
};

export default createStage;
