import { TBotContext } from '../types';

/**
 * @param {TBotContext} ctx контекст
 * @param {number} minLength минимальное количество символов
 * @returns {boolean} возвращает true если сообщение валидное и false если сообщение не валидное
 */
const messageValidation = (ctx: TBotContext, minLength: number): boolean => {
    const isTextMsg = ctx.message?.text !== undefined;
    const isExpectedLength = isTextMsg && ctx.message.text.length >= minLength;
    return isTextMsg && isExpectedLength;
};

export default messageValidation;
