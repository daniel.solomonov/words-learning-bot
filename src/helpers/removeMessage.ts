import { EMsgPos, type TBotContext } from '../types';

/**
 * @param {TBotContext} ctx контекст из родительской функции
 * @param {EMsgPos} msgPos позиция сообщения
 * @returns {Promise} ассинхронная функция, ничего не возвращает кроме Promise
 */
const removeMessage = async (ctx: TBotContext, msgPos: EMsgPos): Promise<void> => {
    if (ctx.message?.message_id !== undefined) {
        await ctx.deleteMessage(ctx.message?.message_id + msgPos);
    }
};

export default removeMessage;
