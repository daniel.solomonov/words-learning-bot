import { Markup } from 'telegraf';

import { Markup } from 'telegraf';

const getMainKeyboard = () => {
    let mainKeyboard = Markup.keyboard([
        ['Movies', 'Settings'],
    ]);
    mainKeyboard = mainKeyboard.oneTime();
    return {
        mainKeyboard
    };
};

const getBackKeyboard = () => {
    let backKeyboard = Markup.keyboard(['Back']);
    backKeyboard = backKeyboard.oneTime();
    return {
        backKeyboard
    };
};

export const getMainMenu = () => {
    return Markup.keyboard([
        ['Мои задачи', 'Добавить задачу'],
        ['Смотивируй меня', 'Удалить задачу']
    ]).resize(); // .extra();
};

export const yesNoKeyboard = () => {
    return Markup.inlineKeyboard([
        Markup.button.callback("👍 Да", "yes"),
        Markup.button.callback("👎 Нет", "no"),
    ], { columns: 2 }); // .extra();
};

export const gradeKeyboard = () => {
    return Markup.inlineKeyboard([
        Markup.button.callback('1', '1'),
        Markup.button.callback('2', '2'),
        Markup.button.callback('3', '3'),
        Markup.button.callback('4', '4'),
        Markup.button.callback('5', '5'),
    ], { columns: 5 }); // .extra();
};

bot.action(['yes', 'no'], ctx => {
    if (ctx.callbackQuery.data === 'yes') {
        addTask(ctx.session.taskText);
        ctx.editMessageText(`Ваша задача успешно добавлена ${ctx.session.taskText}`);
    } else {
        ctx.deleteMessage();
    }
});
