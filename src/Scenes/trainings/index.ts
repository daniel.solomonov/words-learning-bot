import { Scenes } from 'telegraf';
import { message } from 'telegraf/filters';
import { type TBaseScene, type ISceneObject, type TBotContext } from '../../types';
import greeting from './events/greeting';
import { dontKnowBtn, exitBtn } from '../../helpers/buttons';
import { dontKnow, enter, exit, leave, notFound } from './actions';
import showWord from './events/showWord';
import { ETraining } from './events/getRandomTrainingType';
import { tranWordTextCheck, wordTranTextCheck } from './events/trainingsCheck';

export const sceneId = 'TRAININGS_SCENE_ID';

const scene: TBaseScene = new Scenes.BaseScene<TBotContext>(sceneId);

scene.enter(enter);

scene.hears(exitBtn.text, exit);

scene.hears(dontKnowBtn.text, dontKnow);

scene.on(message('text'), async (ctx) => {
    const trainingType = ctx.scene.session.state.trainingType as string;
    switch (trainingType) {
        case ETraining.WordTranText:
            ctx.scene.session.state.prevTrainResMsg = wordTranTextCheck(ctx);
            break;
        case ETraining.TranWordText:
            ctx.scene.session.state.prevTrainResMsg = tranWordTextCheck(ctx);
            break;
        default:
            break;
    }
    await showWord(ctx);
});

scene.leave(leave);

scene.use(notFound);

const trainingsScene: ISceneObject = {
    cmd: 'trainings',
    label: 'Тренировки',
    sceneId: sceneId,
    scene: scene,
    greeting: greeting,
};

export default trainingsScene;
