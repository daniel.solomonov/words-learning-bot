import { type IWordDB } from '../../../../Schemas/word';
import { type TBotContext } from '../../../../types';
import saveWordTraining from '../saveWordTraining';

const tranWordTextCheck = (ctx: TBotContext): string => {
    const randomWord = ctx.scene.session.state.randomWord as IWordDB;
    const userMsgText = ctx.message?.text ?? '';

    if (userMsgText.toUpperCase() === randomWord.word.toUpperCase()) {
        saveWordTraining(ctx, randomWord);
        return 'Верно!\n\n';
    }
    return `Не верно, правильный ответ: ${randomWord.word}\n\n`;
};

export default tranWordTextCheck;
