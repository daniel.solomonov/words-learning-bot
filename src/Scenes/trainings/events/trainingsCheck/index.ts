import tranWordTextCheck from './tranWordTextCheck';
import wordTranTextCheck from './wordTranTextCheck';

export {
    tranWordTextCheck,
    wordTranTextCheck,
};
