
export enum ETraining {
    WordTranText = 'word/translate/text',
    TranWordText = 'translate/word/text',
};

const trainingTypes = [
    ETraining.WordTranText,
    ETraining.TranWordText,
];

const getRandomTrainingType = (): string => {
    const min = 0;
    const max = trainingTypes.length;
    const randomIndex = Math.floor(Math.random() * (max - min) + min);
    return trainingTypes[randomIndex];
};

export default getRandomTrainingType;
