import { type IWordDB } from '../../../Schemas/word';
import { type TBotContext } from '../../../types';

const saveWordTraining = (ctx: TBotContext, word: IWordDB): void => {
    const newWord: IWordDB = {
        ...word,
        progress: word.progress ? word.progress + 5 : 5,
    };
    ctx.session.words = ctx.session.words.map(w => w.id === newWord.id ? newWord : w);
};

export default saveWordTraining;
