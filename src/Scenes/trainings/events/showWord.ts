import { Markup } from 'telegraf';
import { type TBotContext } from '../../../types';
import { dontKnowBtn, exitBtn } from '../../../helpers/buttons';
import getRandomWord from './getRandomWord';
import getRandomTrainingType, { ETraining } from './getRandomTrainingType';

const showWord = async (ctx: TBotContext): Promise<void> => {
    const prevTrainResMsg = ctx.scene.session.state.prevTrainResMsg ?? '';
    const randomWord = getRandomWord(ctx);
    ctx.scene.session.state.randomWord = randomWord;
    const trainingType = getRandomTrainingType();
    ctx.scene.session.state.trainingType = trainingType;
    let randomWordText = '';
    switch (trainingType) {
        case ETraining.WordTranText:
            randomWordText = randomWord.word;
            break;
        case ETraining.TranWordText:
            randomWordText = randomWord.translate;
            break;
        default:
            break;
    }
    await ctx.reply(
        prevTrainResMsg + `Введите перевод слова ${randomWordText}`,
        Markup.keyboard([
            dontKnowBtn.text,
            exitBtn.text,
        ]).resize().oneTime()
    );
};

export default showWord;
