import { type IWordDB } from '../../../Schemas/word';
import { type TBotContext } from '../../../types';

const getRandomWord = (ctx: TBotContext): IWordDB => {
    const words = ctx.session.words;
    const min = 0;
    const max = words.length;
    const randomIndex = Math.floor(Math.random() * (max - min) + min);
    return words[randomIndex];
};

export default getRandomWord;
