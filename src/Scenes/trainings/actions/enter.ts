import { type TMiddleware } from '../../../types';
import showWord from '../events/showWord';

const enter: TMiddleware = async (ctx) => {
    await showWord(ctx);
};

export default enter;
