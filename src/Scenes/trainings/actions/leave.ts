import { Markup } from 'telegraf';
import { type TMiddleware } from '../../../types';

const leave: TMiddleware = async (ctx) => {
    await ctx.reply('Выходим из тренировки...', Markup.removeKeyboard());
};

export default leave;
