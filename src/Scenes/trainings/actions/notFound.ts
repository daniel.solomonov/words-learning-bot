import { type TMiddleware } from '../../../types';

const notFound: TMiddleware = async (ctx) => {
    await ctx.sendMessage('Что то пошло не по сценарию');
};

export default notFound;
