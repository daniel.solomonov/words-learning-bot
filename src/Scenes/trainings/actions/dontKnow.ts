import { type IWordDB } from '../../../Schemas/word';
import { type TMiddleware } from '../../../types';
import { ETraining } from '../events/getRandomTrainingType';
import showWord from '../events/showWord';

const dontKnow: TMiddleware = async (ctx) => {
    const randomWord = ctx.scene.session.state.randomWord as IWordDB;
    const trainingType = ctx.scene.session.state.trainingType as string;
    switch (trainingType) {
        case ETraining.WordTranText:
            ctx.scene.session.state.prevTrainResMsg = `Правильный ответ: ${randomWord.translate}\n\n`;
            break;
        case ETraining.TranWordText:
            ctx.scene.session.state.prevTrainResMsg = `Правильный ответ: ${randomWord.word}\n\n`;
            break;
        default:
            break;
    }
    await showWord(ctx);
};

export default dontKnow;
