import { type TMiddleware } from '../../../types';

const exit: TMiddleware = async (ctx) => {
    return ctx.scene.leave();
};

export default exit;
