import { Scenes } from 'telegraf';
import { type TBaseScene, type ISceneObject, type TBotContext } from '../../types';
import greeting from './events/greeting';
import { exitBtn, nextPageBtn, prevPageBtn, editRegExp, removeRegExp } from '../../helpers/buttons';
import { edit, enter, exit, leave, nextPage, notFound, prevPage, remove } from './actions';

export const sceneId = 'SHOW_WORDS_SCENE_ID';

const scene: TBaseScene = new Scenes.BaseScene<TBotContext>(sceneId);

scene.enter(enter);

scene.action(removeRegExp, remove);

scene.action(editRegExp, edit);

scene.action(prevPageBtn.data, prevPage);

scene.action(nextPageBtn.data, nextPage);

scene.action(exitBtn.data, exit);

scene.leave(leave);

scene.use(notFound);

const showWordsScene: ISceneObject = {
    cmd: 'show_words',
    label: 'Посмотреть список слов',
    sceneId: sceneId,
    scene: scene,
    greeting: greeting,
};

export default showWordsScene;
