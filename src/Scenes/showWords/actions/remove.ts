import { type TMiddleware } from '../../../types';
import removeMsgsList from '../events/removeMsgsList';
import showPortion from '../events/showPortion';

const remove: TMiddleware = async (ctx, next) => {
    await removeMsgsList(ctx, next);
    ctx.session.words = ctx.session.words.filter(word => word.id !== ctx.match[1]);
    await showPortion(ctx, next);
};

export default remove;
