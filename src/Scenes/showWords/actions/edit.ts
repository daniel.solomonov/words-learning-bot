import { type TMiddleware } from '../../../types';
import addWordScene from '../../addWord';

const edit: TMiddleware = async (ctx) => {
    ctx.session.state.wordId = ctx.match[1];
    await ctx.scene.enter(addWordScene.sceneId);
};

export default edit;
