import { type TMiddleware } from '../../../types';

const exit: TMiddleware = async (ctx) => {
    await ctx.sendMessage('exitBtn');
    return ctx.scene.leave();
};

export default exit;
