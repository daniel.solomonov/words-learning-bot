import { type TMiddleware } from '../../../types';
import removeMsgsList from '../events/removeMsgsList';
import showPortion from '../events/showPortion';

const nextPage: TMiddleware = async (ctx, next) => {
    ctx.scene.session.state.portionIndex++;
    await removeMsgsList(ctx, next);
    await showPortion(ctx, next);
};

export default nextPage;
