import { type TMiddleware } from '../../../types';

const leave: TMiddleware = async (ctx) => {
    await ctx.reply('Что-то делаю перед выходом');
};

export default leave;
