import { type TMiddleware } from '../../../types';
import showPortion from '../events/showPortion';

const enter: TMiddleware = async (ctx, next) => {
    ctx.scene.session.state.portionIndex = 0;
    ctx.scene.session.state.msgIds = [];
    await showPortion(ctx, next);
};

export default enter;
