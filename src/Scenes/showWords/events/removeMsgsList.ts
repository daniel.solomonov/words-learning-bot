import { type TMiddleware } from '../../../types';

const removeMsgsList: TMiddleware = async (ctx) => {
    const msgIds = ctx.scene.session.state.msgIds;
    if (Array.isArray(msgIds)) {
        for (let i = 0; i < msgIds.length; i++) {
            await ctx.deleteMessage(msgIds[i]);
        }
    }
};

export default removeMsgsList;
