import { type TMiddleware } from '../../../types';
import { editBtn, exitBtn, nextPageBtn, prevPageBtn, removeBtn } from '../../../helpers/buttons';
import { replyMarkup } from '../replyMarkup';
import displayWord from '../../../helpers/displayWord';

const portionOnPage = 2;

const showPortion: TMiddleware = async (ctx) => {
    const words = ctx.session.words;
    ctx.scene.session.state.msgIds = [];
    if (words.length > 0) {
        const { portionIndex } = ctx.scene.session.state;
        const startIndex = portionIndex * portionOnPage;
        const portionExit = (portionIndex + 1) * portionOnPage;
        for (let i = startIndex; i < words.length && i < portionExit; i++) {
            const isStartOfList = portionIndex === 0;
            const isEndPortion = i + 1 === portionExit;
            const isEndOfList = i + 1 === words.length;
            const { id } = words[i];
            const textMessage = displayWord(words[i]);
            const btnsLine1 = [editBtn(id), removeBtn(id)];
            const btnsLine2 = [];
            const btnsLine3 = [exitBtn];
            if (!isStartOfList && (isEndOfList || isEndPortion)) {
                btnsLine2.push(prevPageBtn);
            }
            if (!isEndOfList) {
                btnsLine2.push(nextPageBtn);
            }
            const msg = await ctx.reply(
                textMessage,
                isEndPortion || isEndOfList
                    ? replyMarkup(btnsLine1, btnsLine2, btnsLine3)
                    : replyMarkup(btnsLine1)
            );
            const msgIds = ctx.scene.session.state.msgIds;
            if (Array.isArray(msgIds)) {
                msgIds.push(msg.message_id);
            }
        }
    } else {
        await ctx.sendMessage('Вы еще не добавили ни одно слово...');
        return ctx.scene.leave();
    }
};

export default showPortion;
