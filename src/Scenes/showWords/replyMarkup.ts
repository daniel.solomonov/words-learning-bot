import { Markup } from 'telegraf';
import { type IButton } from '../../types';

export const replyMarkup = (...btns: IButton[][]) => {
    return {
        reply_markup: {
            inline_keyboard: btns.map(btnInner => btnInner.map(btn => getCallback(btn))),
        }
    }
};

const getCallback = (btn: IButton) => {
    const { text, data, hide } = btn;
    return Markup.button.callback(text, data, hide);
};
