import { type TMiddleware } from '../../../types';

const exit: TMiddleware = async (ctx) => {
    await ctx.sendMessage('exit');
    return ctx.scene.leave();
};

export default exit;
