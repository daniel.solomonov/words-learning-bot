import { exitBtn } from '../../../helpers/buttons';
import { type TMiddleware } from '../../../types';
import { replyMarkup } from '../../showWords/replyMarkup';

const enter: TMiddleware = async (ctx) => {
    await ctx.reply(
        'enter',
        replyMarkup([exitBtn])
    );
};

export default enter;
