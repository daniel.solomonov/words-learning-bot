import { Input, Scenes } from 'telegraf';
import { type TBaseScene, type ISceneObject, type TBotContext } from '../../types';
import greeting from './events/greeting';
import { exitBtn } from '../../helpers/buttons';
import { feedback_msg } from '../../Events/feedback/assessmentKeyboard';
import { enter, exit, leave, notFound } from './actions';
import { Message } from 'telegraf/types';
import envParams from '../../helpers/envParams';

/////////////////////////////////////////////////////

const getMessageType = (message: any) => {
    const keys = Object.keys(message);
    console.log('keys', keys);

    const messageType = keys[keys.length - 1] === 'caption' ? keys[keys.length - 2] : keys[keys.length - 1];
    return messageType;
};

/////////////////////////////////////////////////////

export const sceneId = 'SUPPORT_SCENE_ID';

const scene: TBaseScene = new Scenes.BaseScene<TBotContext>(sceneId);

scene.enter(enter);

scene.on('message', async (ctx) => {
    const myUserId = envParams.MY_USER_ID;
    const msgType = getMessageType(ctx.message);
    console.log('msgType', msgType);
    let userMsg;
    // keys [ 'message_id', 'from', 'chat', 'date', 'media_group_id', 'video' ]
    //
    // text -> text
    // gif -> ['animation', 'document']
    // sticker -> sticker
    //
    //
    // (фото или видео) photo -> photo
    // (фото или видео) 2 photo -> ['media_group_id', 'photo'] * 2
    // (фото или видео) video -> video
    // (фото или видео) 2 video -> ['media_group_id', 'video'] * 2
    // (фото или видео) video + photo -> ['media_group_id', 'video'] + ['media_group_id', 'photo']
    // (фото или видео) video + photo + text -> ['media_group_id', 'video'] + ['media_group_id', 'photo', 'caption']
    //
    //
    // (файл) video + photo -> ['media_group_id', 'document'] * 2
    // (файл) txt file + text -> ['document', 'caption']
    switch (msgType) {
        case 'text':
            userMsg = ctx.message as Message.TextMessage;
            await ctx.telegram.sendMessage(myUserId, 'User: ' + userMsg.text);
            break;
        case 'sticker':
            userMsg = ctx.message as Message.StickerMessage;
            await ctx.telegram.sendSticker(myUserId, userMsg.sticker.file_id);
            break;
        case 'document':
            userMsg = ctx.message as Message.DocumentMessage;
            await ctx.telegram.sendDocument(myUserId, userMsg.document.file_id, { caption: userMsg.caption });
            break;
        case 'photo':
            userMsg = ctx.message as Message.PhotoMessage;
            await ctx.telegram.sendPhoto(myUserId, Input.fromFileId(userMsg.photo[0].file_id), { caption: userMsg.caption });
            break;
        case 'video':
            userMsg = ctx.message as Message.VideoMessage;
            await ctx.telegram.sendVideo(myUserId, Input.fromFileId(userMsg.video.file_id), { caption: userMsg.caption });
            break;
        case 'audio':
            userMsg = ctx.message as Message.AudioMessage;
            await ctx.telegram.sendAudio(myUserId, Input.fromFileId(userMsg.audio.file_id), { caption: userMsg.caption });
            break;
        // case 'caption':
        //     userMsg = ctx.message as Message.CaptionableMessage;
        //     console.log(userMsg);

        //     await ctx.telegram.send(myUserId, Input.fromFileId(userMsg.audio.file_id));
        //     break;
        default:
            break;
    }
    //
});

scene.action(exitBtn.data, exit);

scene.leave(leave);

scene.use(notFound);

const supportScene: ISceneObject = {
    cmd: feedback_msg.data,
    label: 'Написать в тех. поддержку',
    sceneId: sceneId,
    scene: scene,
    greeting: greeting,
};

export default supportScene;
