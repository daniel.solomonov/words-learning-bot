import supportScene from '..';
import { exitBtn } from '../../../helpers/buttons';
import { type TMiddleware } from '../../../types';
import { replyMarkup } from '../../showWords/replyMarkup';

const greeting: TMiddleware = async (ctx) => {
    await ctx.scene.enter(supportScene.sceneId);
};

export default greeting;
