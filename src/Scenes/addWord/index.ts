import { Scenes } from 'telegraf';
import { type ISceneObject, type TBotContext, type TWizardScene } from '../../types';
import greeting from './events/greeting';
import steps from './steps';
import { backBtn, cancelBtn, saveBtn, skipBtn } from '../../helpers/buttons';
import { back, cancel, leave, save, skip } from './actions';

export const sceneId = 'ADD_WORD_SCENE_ID';

const scene: TWizardScene = new Scenes.WizardScene<TBotContext>(
	sceneId,
	...steps,
);

scene.action(cancelBtn.data, cancel);

scene.action(backBtn.data, back);

scene.action(skipBtn.data, skip);

scene.action(saveBtn.data, save);

scene.leave(leave);

const addWordScene: ISceneObject = {
	cmd: 'add_word',
	label: 'Добавить слово',
	sceneId: sceneId,
	scene: scene,
	greeting: greeting,
};

export default addWordScene;
