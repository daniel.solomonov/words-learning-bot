import { sceneId } from '../';
import { type TMiddleware } from '../../../types';

const greeting: TMiddleware = async (ctx) => {
    await ctx.sendMessage('Стартуем...');
    await ctx.scene.enter(sceneId);
};

export default greeting;
