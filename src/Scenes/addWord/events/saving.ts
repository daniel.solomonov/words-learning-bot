import { v4 as uuid } from 'uuid';
import { type TMiddleware } from '../../../types';
import { IWordDB } from '../../../Schemas/word';
import displayWord from '../../../helpers/displayWord';

const saving: TMiddleware = async (ctx) => {
    const { word, translate, additionalTranslate, description, example } = ctx.scene.session.word;
    ctx.scene.session.word = {};
    const wordId = ctx.session.state?.wordId;
    if (word && translate) {
        const newWord: IWordDB = {
            id: typeof wordId === 'string' ? wordId : uuid(),
            word,
            translate,
            additionalTranslate,
            description,
            example,
        };
        if (wordId) {
            ctx.session.words = ctx.session.words.map(word => word.id === wordId ? newWord : word);
        } else {
            ctx.session.words.push(newWord);
        }
        const textMessage = (wordId ? 'Слово изменено:\n' : 'Слово добавлено:\n') + displayWord(newWord);
        await ctx.sendMessage(textMessage);
        await ctx.sendMessage('Сохранение...');
    }
};

export default saving;
