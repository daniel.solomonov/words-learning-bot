import { goToExit } from '../../../helpers/wizardSteps';
import { type TMiddleware } from '../../../types';
import saving from '../events/saving';

const save: TMiddleware = async (ctx, next) => {
    await ctx.deleteMessage();
    await saving(ctx, next);
    return await goToExit(ctx);
};

export default save;
