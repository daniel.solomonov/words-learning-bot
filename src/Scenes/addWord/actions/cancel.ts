import { goToExit } from '../../../helpers/wizardSteps';
import { type TMiddleware } from '../../../types';

const cancel: TMiddleware = async (ctx) => {
    await ctx.deleteMessage();
    return await goToExit(ctx);
};

export default cancel;
