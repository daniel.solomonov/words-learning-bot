import { goToNext } from '../../../helpers/wizardSteps';
import { type TMiddleware } from '../../../types';

const skip: TMiddleware = async (ctx, next) => {
    await ctx.deleteMessage();
    return await goToNext(ctx, next);
};

export default skip;
