import { type TMiddleware } from '../../../types';

const leave: TMiddleware = async (ctx) => {
    ctx.session.state = {};
    await ctx.sendMessage('Выход...');
};

export default leave;
