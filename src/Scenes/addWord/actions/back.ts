import { goToPlusStep } from '../../../helpers/wizardSteps';
import { EMsgPos, type TMiddleware } from '../../../types';

const back: TMiddleware = async (ctx, next) => {
    if (typeof ctx.scene.session.state.msgId === 'number') {
        await ctx.deleteMessage(ctx.scene.session.state.msgId - 1);
        await ctx.deleteMessage(ctx.scene.session.state.msgId);
        await ctx.deleteMessage(ctx.scene.session.state.msgId + 1);
    }
    return await goToPlusStep(ctx, next, EMsgPos.prev3);
};

export default back;
