import keyboard from '../../examples/keyboard';
import { type TResultKeyboard, type IButton } from '../../types';

/**
 * @param {IButton[]} btns одна или несколько кнопок типа IButton через запятую
 * @returns {Promise} ассинхронная функция, ничего не возвращает кроме Promise
 */
export const replyMarkup = (...btns: IButton[]): TResultKeyboard => {
    return keyboard({
        btns: btns,
        columns: 2,
    });
};
