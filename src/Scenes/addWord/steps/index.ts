import wordFn from './wordFn';
import translateFn from './translateFn';
import additTranslFn from './additTranslFn';
import descriptionFn from './descriptionFn';
import exampleFn from './exampleFn';
import endFn from './endFn';

const steps = [
    ...wordFn,
    ...translateFn,
    ...additTranslFn,
    ...descriptionFn,
    ...exampleFn,
    endFn,
];

export default steps;
