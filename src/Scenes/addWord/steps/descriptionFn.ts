import messageValidation from '../../../helpers/messageValidation';
import { goToNext } from '../../../helpers/wizardSteps';
import { EMsgPos, type TMiddleware } from '../../../types';
import editMessage from '../../../helpers/editMessage';
import removeMessage from '../../../helpers/removeMessage';
import { backBtn, cancelBtn, saveBtn, skipBtn } from '../../../helpers/buttons';
import { replyMarkup } from '../replyMarkup';

const descriptionEnter: TMiddleware = async (ctx) => {
    let msgText = 'Введите описание на русском:';
    const wordId = ctx.session.state?.wordId;
    if (wordId) {
        msgText = 'Измените описание на русском: ' + ctx.session.words.find(word => word.id === wordId)?.description;
    }
    await ctx.reply(
        msgText,
        replyMarkup(cancelBtn, backBtn, skipBtn, saveBtn),
    );
    ctx.scene.session.state.msgId = ctx.message?.message_id;
    return ctx.wizard.next();
};

const descriptionCheck: TMiddleware = async (ctx, next) => {
    const wordId = ctx.session.state?.wordId;
    let msgText = 'Введите нормально описание на русском:';
    const isMsgCorrect = messageValidation(ctx, 3);
    if (!isMsgCorrect) {
        if (wordId) {
            msgText = 'Измените нормально описание на русском: ' + ctx.session.words.find(word => word.id === wordId)?.description;
        }
        await removeMessage(ctx, EMsgPos.prev);
        await removeMessage(ctx, EMsgPos.current);
        await ctx.reply(
            msgText,
            replyMarkup(cancelBtn, backBtn, skipBtn, saveBtn),
        );
        return;
    }
    msgText = 'Введенное описание на русском:';
    if (wordId) {
        msgText = 'Измененное описание на русском:';
    }
    await editMessage(ctx, EMsgPos.prev, msgText);
    ctx.scene.session.word.description = ctx.message?.text ?? '';
    ctx.scene.session.state.msgId = ctx.message?.message_id;
    return await goToNext(ctx, next);
};

export default [descriptionEnter, descriptionCheck];
