import messageValidation from '../../../helpers/messageValidation';
import { goToNext } from '../../../helpers/wizardSteps';
import { EMsgPos, type TMiddleware } from '../../../types';
import editMessage from '../../../helpers/editMessage';
import removeMessage from '../../../helpers/removeMessage';
import { backBtn, cancelBtn } from '../../../helpers/buttons';
import { replyMarkup } from '../replyMarkup';

const translateEnter: TMiddleware = async (ctx) => {
    let msgText = 'Введите перевод на русском:';
    const wordId = ctx.session.state?.wordId;
    if (wordId) {
        msgText = 'Измените перевод на русском: ' + ctx.session.words.find(word => word.id === wordId)?.translate;
    }
    await ctx.reply(
        msgText,
        replyMarkup(cancelBtn, backBtn),
    );
    ctx.scene.session.state.msgId = ctx.message?.message_id;
    return ctx.wizard.next();
};

const translateCheck: TMiddleware = async (ctx, next) => {
    const wordId = ctx.session.state?.wordId;
    let msgText = 'Введите нормально перевод на русском:';
    const isMsgCorrect = messageValidation(ctx, 3);
    if (!isMsgCorrect) {
        if (wordId) {
            msgText = 'Измените нормально перевод на русском: ' + ctx.session.words.find(word => word.id === wordId)?.translate;
        }
        await removeMessage(ctx, EMsgPos.prev);
        await removeMessage(ctx, EMsgPos.current);
        await ctx.reply(
            msgText,
            replyMarkup(cancelBtn, backBtn),
        );
        return;
    }
    msgText = 'Введенное слово на русском:';
    if (wordId) {
        msgText = 'Измененное слово на русском:';
    }
    await editMessage(ctx, EMsgPos.prev, msgText);
    ctx.scene.session.word.translate = ctx.message?.text ?? '';
    ctx.scene.session.state.msgId = ctx.message?.message_id;
    return await goToNext(ctx, next);
};

export default [translateEnter, translateCheck];

