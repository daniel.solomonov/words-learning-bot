import messageValidation from '../../../helpers/messageValidation';
import { goToNext } from '../../../helpers/wizardSteps';
import { EMsgPos, type TMiddleware } from '../../../types';
import editMessage from '../../../helpers/editMessage';
import removeMessage from '../../../helpers/removeMessage';
import { backBtn, cancelBtn, saveBtn, skipBtn } from '../../../helpers/buttons';
import { replyMarkup } from '../replyMarkup';

const additTranslEnter: TMiddleware = async (ctx) => {
    let msgText = 'Введите дополнительный перевод на русском:';
    const wordId = ctx.session.state?.wordId;
    if (wordId) {
        msgText = 'Измените дополнительный перевод на русском: ' + ctx.session.words.find(word => word.id === wordId)?.additionalTranslate;
    }
    await ctx.reply(
        msgText,
        replyMarkup(cancelBtn, backBtn, skipBtn, saveBtn),
    );
    ctx.scene.session.state.msgId = ctx.message?.message_id;
    return ctx.wizard.next();
};

const additTranslCheck: TMiddleware = async (ctx, next) => {
    const wordId = ctx.session.state?.wordId;
    let msgText = 'Введите нормально дополнительный перевод на русском:';
    const isMsgCorrect = messageValidation(ctx, 3);
    if (!isMsgCorrect) {
        if (wordId) {
            msgText = 'Измените нормально дополнительный перевод на русском: ' + ctx.session.words.find(word => word.id === wordId)?.additionalTranslate;
        }
        await removeMessage(ctx, EMsgPos.prev);
        await removeMessage(ctx, EMsgPos.current);
        await ctx.reply(
            msgText,
            replyMarkup(cancelBtn, backBtn, skipBtn, saveBtn),
        );
        return;
    }
    msgText = 'Введенный дополнительный перевод на русском:';
    if (wordId) {
        msgText = 'Измененный дополнительный перевод на русском:';
    }
    await editMessage(ctx, EMsgPos.prev, msgText);
    ctx.scene.session.word.additionalTranslate = ctx.message?.text ?? '';
    ctx.scene.session.state.msgId = ctx.message?.message_id;
    return await goToNext(ctx, next);
};

export default [additTranslEnter, additTranslCheck];
