import messageValidation from '../../../helpers/messageValidation';
import { goToNext } from '../../../helpers/wizardSteps';
import { EMsgPos, type TMiddleware } from '../../../types';
import editMessage from '../../../helpers/editMessage';
import removeMessage from '../../../helpers/removeMessage';
import { backBtn, cancelBtn, saveBtn, skipBtn } from '../../../helpers/buttons';
import { replyMarkup } from '../replyMarkup';

const exampleEnter: TMiddleware = async (ctx) => {
    let msgText = 'Введите пример на английском:';
    const wordId = ctx.session.state?.wordId;
    if (wordId) {
        msgText = 'Измените пример на английском: ' + ctx.session.words.find(word => word.id === wordId)?.example;
    }
    await ctx.reply(
        msgText,
        replyMarkup(cancelBtn, backBtn, skipBtn, saveBtn),
    );
    ctx.scene.session.state.msgId = ctx.message?.message_id;
    return ctx.wizard.next();
};

const exampleCheck: TMiddleware = async (ctx, next) => {
    const wordId = ctx.session.state?.wordId;
    let msgText = 'Введите нормально пример на английском:';
    const isMsgCorrect = messageValidation(ctx, 3);
    if (!isMsgCorrect) {
        if (wordId) {
            msgText = 'Измените нормально пример на английском: ' + ctx.session.words.find(word => word.id === wordId)?.example;
        }
        await removeMessage(ctx, EMsgPos.prev);
        await removeMessage(ctx, EMsgPos.current);
        await ctx.reply(
            msgText,
            replyMarkup(cancelBtn, backBtn, skipBtn, saveBtn),
        );
        return;
    }
    msgText = 'Введенный пример на английском:';
    if (wordId) {
        msgText = 'Измененный пример на английском:';
    }
    await editMessage(ctx, EMsgPos.prev, msgText);
    ctx.scene.session.word.example = ctx.message?.text ?? '';
    ctx.scene.session.state.msgId = ctx.message?.message_id;
    return await goToNext(ctx, next);
};

export default [exampleEnter, exampleCheck];
