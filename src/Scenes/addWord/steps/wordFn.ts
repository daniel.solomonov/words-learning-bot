import { EMsgPos, type TMiddleware } from '../../../types';
import messageValidation from '../../../helpers/messageValidation';
import { goToNext } from '../../../helpers/wizardSteps';
import removeMessage from '../../../helpers/removeMessage';
import editMessage from '../../../helpers/editMessage';
import { cancelBtn } from '../../../helpers/buttons';
import { replyMarkup } from '../replyMarkup';

const wordEnter: TMiddleware = async (ctx) => {
    ctx.scene.session.word = {};
    let msgText = 'Введите слово на английском:';
    const wordId = ctx.session.state?.wordId;
    if (typeof wordId === 'string') {
        msgText = 'Измените слово на английском: ' + ctx.session.words.find(word => word.id === wordId)?.word;
    }
    await ctx.reply(
        msgText,
        replyMarkup(cancelBtn),
    );
    ctx.scene.session.state.msgId = ctx.message?.message_id;
    return ctx.wizard.next();
};

const wordCheck: TMiddleware = async (ctx, next) => {
    const wordId = ctx.session.state?.wordId;
    let msgText = 'Введите нормально слово на английском:';
    const isMsgCorrect = messageValidation(ctx, 3);
    if (!isMsgCorrect) {
        if (wordId) {
            msgText = 'Измените нормально слово на английском: ' + ctx.session.words.find(word => word.id === wordId)?.word;
        }
        await removeMessage(ctx, EMsgPos.prev);
        await removeMessage(ctx, EMsgPos.current);
        await ctx.reply(
            msgText,
            replyMarkup(cancelBtn),
        );
        return;
    }
    msgText = 'Введенное слово на английском:';
    if (wordId) {
        msgText = 'Измененное слово на английском:';
    }
    await editMessage(ctx, EMsgPos.prev, msgText);
    ctx.scene.session.word.word = ctx.message?.text ?? '';
    ctx.scene.session.state.msgId = ctx.message?.message_id;
    return await goToNext(ctx, next);
};

export default [wordEnter, wordCheck];
