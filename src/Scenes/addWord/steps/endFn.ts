import { type TMiddleware } from '../../../types';
import saving from '../events/saving';

const endFn: TMiddleware = async (ctx, next) => {
    await saving(ctx, next);
    await ctx.sendMessage('Завершение...');
    return await ctx.scene.leave();
};

export default endFn;
